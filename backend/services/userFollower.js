const db = require('./db');

async function userFollower(id) {

    try {
        const rows = db.query("select avatar, login_user from users u , abonnements a where u.id_user = a.id_user_follower AND a.id_user = ? and u.id_user <> a.id_user;", [id]);
        return rows;
    } catch (err) {
        console.log(`Error to get user's follower`, err.message);
        next(err);
    }

}

module.exports = {
    userFollower
}
