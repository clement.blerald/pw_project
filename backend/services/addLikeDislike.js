const db = require('./db');


//create function for getting user's publications after login 
async function addReactions(idPublication, id, idEmote) {

    // Another way to add like and dislke in node.js
    //     try {
    //         const rows = db.query(`INSERT INTO pw_projet.reactions(id_publication, id_user, id_emote)
    // select ${idPublication}, ${id}, ${idEmote}
    // where not exists(select 1 from reactions as r where r.id_user = ${id} and r.id_publication = ${idPublication} and r.id_emote = ${idEmote})`);
    //         return rows;

    //     } catch (e) {
    //         return { err: e }
    //     }

    //Add like et dislike via database function : PROCEDURE pw_projet.insert_reac
    try {
        const rows = db.query(`call pw_projet.insert_reac (?,?,?)`, [idPublication, id, idEmote]);
        return rows;

    } catch (err) {
        console.log(`Error to add reactions`, err.message);
        next(err);
    }



}
module.exports = {
    addReactions
}