const db = require('./db');


//create function for getting user's follower after login 
async function getFollowerList(login) {

    const rows = db.query(`SELECT u2.id_user, u2.avatar, u2.login_user
                           FROM abonnements a natural join users u2
                        WHERE a.id_user_follower = (select u.id_user
                                                    from users u
                                                    where u.login_user=?);`, [login])
    return rows;

}

module.exports = {
    getFollowerList
}

// SELECT u2.id_user, u2.avatar, u2.login_user
// FROM abonnements a natural join users u2
// WHERE a.id_user_follower = (select u.id_user
// from users u
// where u.login_user = 'Piccolo');