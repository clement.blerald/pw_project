const db = require('./db');

async function getById(uid) { //get user by id
    try {
        const rows = db.query("SELECT * FROM users WHERE id_user= ?", [uid])
        return rows;
    } catch (err) {
        console.log("Error to get user's id ");
        return { err: e }
    }

}

module.exports = {
    getById
}
