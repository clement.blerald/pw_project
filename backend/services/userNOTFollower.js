const db = require('./db');

//create function for login 
async function userNOTFollower(id) {
    try {

        const rows = db.query(`SELECT avatar, login_user
        FROM users u1 WHERE u1.id_user NOT IN (
            select u.id_user FROM users u JOIN abonnements a 
            ON a.id_user = u.id_user AND a.id_user_follower = ?
        ) AND u1.id_user != ?;`, [id, id]);
        return rows;

    } catch (err) {
        console.log("Error to get user's post ");
        return { err: e }
    }
}

module.exports = {
    userNOTFollower
}





