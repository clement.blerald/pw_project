const db = require('./db');


//create function for getting user's publications after login 
async function getUserList() {

    try {
        const rows = db.query(`SELECT id_user, avatar, login_user FROM users`)
        return rows;

    } catch (err) {
        console.log("Error to get all user list");
        return { err: e }
    }


}
module.exports = {
    getUserList
}