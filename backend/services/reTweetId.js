const db = require('./db');

async function getReTweetId(idPublication) { //get user by id

    try {
        const rows = db.query(`select u.avatar, u.login_user,
    date_format(r.retweet_date, "%D %b %Y %H:%i") retweet_date, r.retweets
from retweets r, users u
where u.id_user = r.id_user
    AND id_publication = ?;`, [idPublication])
        return rows;
    } catch (err) {
        console.log("Error to get retweet post");
        return { err: e }
    }
}

module.exports = {
    getReTweetId
}
