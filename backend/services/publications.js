const db = require('./db');


async function getPublications() {
    try {
        const rows = db.query(`with emo as
        (SELECT p.id_publication,
                p.id_user,
                p.create_date,
                p.publication,
                p.is_public,
                COALESCE(sum(case
                                 when id_emote = 1 then 1
                             end),0) as likee,
                COALESCE(sum(case
                                 when id_emote = 2 then 1
                             end),0) as dislike
         FROM pw_projet.publications p
         left outer join pw_projet.reactions r on p.id_publication=r.id_publication
         GROUP BY id_publication)
    SELECT u.avatar, u.login_user,
           date_format(e.create_date, "%D %b %Y %H:%i") create_date,
           e.publication,
           e.likee,
           e.dislike,
           e.id_publication
    FROM users u,
         emo e
    WHERE u.id_user = e.id_user
        AND e.is_public = true
    ORDER BY e.create_date desc;`)
        return rows;

    } catch (err) {
        console.log("Error to get all post ");
        return { err: e }
    }
}

module.exports = {
    getPublications
}