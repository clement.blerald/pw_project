const db = require('./db');


//create function for getting user's publications after login 
async function getUserAndAllPublications(login, login) {

    try {
        const rows = db.query(`    with emo as
    (SELECT p.id_publication,
            p.id_user,
            p.create_date,
            p.publication,
            p.is_public,
            COALESCE(sum(case
                             when id_emote = 1 then 1
                         end),0) as likee,
            COALESCE(sum(case
                             when id_emote = 2 then 1
                         end),0) as dislike
     FROM pw_projet.publications p
     left outer join pw_projet.reactions r on p.id_publication=r.id_publication
     GROUP BY id_publication)
select avatar, login_user,  date_format(create_date, "%D %b %Y %H:%i") create_date, publication, likee, dislike, id_publication
from (
SELECT u.avatar,
       u.login_user,
       e.create_date,
       publication,
       e.likee,
       e.dislike,
       e.id_publication
FROM pw_projet.users u,
     emo e
WHERE u.id_user = e.id_user
    AND is_public = true
UNION
SELECT u.avatar,
       u.login_user,
       e.create_date,
       e.publication,
       e.likee,
       e.dislike,
       e.id_publication
FROM pw_projet.users u,
     pw_projet.mentions m,
     emo e
WHERE m.id_publication = e.id_publication
    AND u.id_user = e.id_user
    AND m.login_user = CONCAT("@", ?)
UNION
SELECT u.avatar,
       u.login_user,
       e.create_date,
       e.publication,
       e.likee,
       e.dislike,
       e.id_publication
FROM pw_projet.abonnements a,
     pw_projet.users u,
     emo e
WHERE u.id_user = a.id_user
    AND u.id_user = e.id_user
    AND a.id_user_follower =
        (select id_user
         from users
         where login_user=?)
) a
ORDER BY a.create_date desc, a.publication desc;`, [login, login])
        return rows;
    } catch (err) {
        console.log("Error to get user's post ");
        return { err: e }
    }
}

module.exports = {
    getUserAndAllPublications
}
