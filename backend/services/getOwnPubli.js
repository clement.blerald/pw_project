const db = require('./db');

//create function for getting user's follower after login 
async function getOwnPubli(id) {
    try {
        const rows = db.query(`
    SELECT id_publication, u.avatar, u.login_user,
    date_format(p.create_date, "%D %b %Y %H:%i") create_date, p.publication 
    FROM users u JOIN publications p
    WHERE u.id_user = ? AND p.id_user = ? order by p.create_date desc;`, [id, id]);
        return rows;
    } catch (err) {
        console.log("Error to ger user connected's posts");
        return { err: e }
    }

}

module.exports = {
    getOwnPubli
}