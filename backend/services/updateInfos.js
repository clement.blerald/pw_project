const db = require('./db');

//create function for login 
async function updateInfos(data) {
    try {
        data = JSON.parse(data);
        if (data['login'] != undefined) {
            var login = `UPDATE users SET login_user = '`.concat(data['login']).concat(`' WHERE id_user = `).concat(data['user_id']).concat(`; `)
            db.query(login)
        }

        if (data['email'] != undefined) {
            var email = `UPDATE users SET email = '`.concat(data['email']).concat(`' WHERE id_user = `).concat(data['user_id']).concat('; ')
            db.query(email)
        }

        if (data['intro'] != undefined) {
            var intro = `UPDATE users SET introduction = '`.concat(data['intro']).concat(`' WHERE id_user = `).concat(data['user_id']).concat(';')
            db.query(intro)
        }

        // console.log("login=", login);
        // console.log("email=", email);
        // console.log("intro=", intro);

        return "OK";

    } catch (err) {
        console.log(`Error to get query updateInfos`, err.message);
        next(err);
    }

}

module.exports = {
    updateInfos
}
