const db = require('./db');

async function deletedPosts(idPublication, idUser) {

    try {

        const rows = db.query(`call pw_projet.delete_pub(${idPublication}, ${idUser});`);
        // console.log("idp=", idPublication)
        // console.log("idu=", idUser)

        // console.log("deleted OK services")
        return rows;

    } catch (err) {
        console.log(`Error to deleted post services`, err.message);
        next(err);
    }

}

module.exports = {
    deletedPosts
}
