const db = require('./db');

//create function for login 
async function subscribeTo(login_user, follower) {
    try {
        const tmp = await db.query(`SELECT id_user,login_user FROM users WHERE login_user = ? OR login_user = ?;`, [follower, login_user]);

        id1 = tmp[0].login_user == login_user ? tmp[0].id_user : tmp[1].id_user;//follow
        id2 = tmp[1].login_user == follower ? tmp[1].id_user : tmp[0].id_user;//user
        // console.log("t0=", tmp[0], "t1=", tmp[1]);
        // console.log("user=", id1, "follower=", id2);

        const rows = db.query(`INSERT INTO abonnements(id_user,id_user_follower) VALUES(?,?);`,
            [id1, id2]);
        return rows;

    } catch (err) {
        console.log("Error to subscribe new users");
        return { err: e }
    }

}

module.exports = {
    subscribeTo
}

