const db = require('./db');


//create function for getting user's publications after login 
async function addRetweets(idPublication, id, retweets) {

    try {

        const rows = db.query("INSERT INTO pw_projet.retweets(id_publication, id_user, retweet_date, retweets) VALUES(?, ?, now(),?)", [idPublication, id, retweets]);
        return rows;

    } catch (err) {
        console.log("Error to add retweet");
        return { err: e }
    }

}

module.exports = {
    addRetweets
}