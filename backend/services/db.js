//db.js for connecting to database mysql
const mysql = require('mysql2/promise');
const config = require('../config');

async function query(sql, params) { //function query is going to create sql request
    try {
        const connection = await mysql.createConnection(config.db); //createConnection search in config.db which we defined in config.js
        const [results,] = await connection.execute(sql, params); //result exec sql and params
        connection.end();
        // console.log("dbdbdb");
        // console.log("results=", results)
        return results;

    } catch (err) {

        console.log("Error to create connection to database, err=", err);
        return { err: e }
    }

}

module.exports = {
    query
}
