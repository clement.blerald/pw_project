DROP DATABASE IF EXISTS pw_projet;
CREATE DATABASE pw_projet;
USE pw_projet;
-- set global max_connections = 5000;
DROP user IF EXISTS 'pw_projet'@'localhost';
CREATE user 'pw_projet'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';
GRANT ALL PRIVILEGES ON *.*  TO 'pw_projet'@'localhost';
flush privileges;

DROP TABLE IF EXISTS pw_projet.reactions CASCADE;
DROP TABLE IF EXISTS pw_projet.emote CASCADE;
DROP TABLE IF EXISTS pw_projet.retweets CASCADE;
DROP TABLE IF EXISTS pw_projet.hashtags CASCADE;
DROP TABLE IF EXISTS pw_projet.mentions CASCADE;
DROP TABLE IF EXISTS pw_projet.publications CASCADE;
DROP TABLE IF EXISTS pw_projet.abonnements CASCADE;
DROP TABLE IF EXISTS pw_projet.users CASCADE;



CREATE TABLE users ( 
    id_user INT NOT NULL AUTO_INCREMENT,
    login_user VARCHAR(50) UNIQUE NOT NULL, 
    password_user VARCHAR(50) NOT NULL,
    avatar TEXT,        /* photo_url after login*/
    email VARCHAR(50),   /* for profile after login*/
    introduction TEXT, /*for profile*/
    PRIMARY KEY(id_user)
);


CREATE TABLE abonnements (
    id_abonnement INT NOT NULL AUTO_INCREMENT, 
    id_user INT NOT NULL,             /*id author*/
    id_user_follower INT NOT NULL, /*id follower*/
    -- CHECK(id_user != id_user_follower),
    FOREIGN KEY (id_user) REFERENCES users (id_user),
    FOREIGN KEY (id_user_follower) REFERENCES users (id_user),
    PRIMARY KEY(id_abonnement)
);


-- Publications et referencement
-- Lors des publications, remplissage auto des tables Mention et hashtag via trigger ou nodejs
CREATE TABLE publications ( 
    id_publication INT NOT NULL AUTO_INCREMENT, 
    id_user INT NOT NULL, 
    create_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    publication TEXT NOT NULL, 
    is_public boolean NOT NULL default false,
    PRIMARY KEY(id_publication),
    FOREIGN KEY (id_user) REFERENCES users (id_user)
);

CREATE TABLE mentions (
	id_publication INT NOT NULL,
	login_user VARCHAR(50) NOT NULL,
 --	FOREIGN KEY (login_user) REFERENCES users (login_user),
	FOREIGN KEY (id_publication) REFERENCES publications (id_publication)
);

CREATE TABLE hashtags (
    id_publication  INT NOT NULL,
	hashtag VARCHAR(50)  NOT NULL,
	FOREIGN KEY (id_publication) REFERENCES publications (id_publication)
);

-- discutions et reactions
-- on peut retweet plusieurs fois a une publication
CREATE TABLE retweets (
	id_retweet INT NOT NULL AUTO_INCREMENT,
	id_publication INT NOT NULL,
	id_user INT NOT NULL,
    retweet_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	retweets TEXT NOT NULL,
    PRIMARY KEY(id_retweet),
	FOREIGN KEY (id_publication) REFERENCES publications (id_publication),
    FOREIGN KEY (id_user) REFERENCES users (id_user)
);


-- Emotes
-- liste les emotions ( like dislike cry happy... )
CREATE TABLE emote (
	id_emote INTEGER NOT NULL,
	emote VARCHAR(10) NOT NULL,
    PRIMARY KEY(id_emote)
);

-- un user ne peut mettre qu'une emotion de type  like ou un dislike par publication => unique pub+user
CREATE TABLE reactions (
    id_publication INT NOT NULL,
	id_user INT NOT NULL,
	id_emote INT NOT NULL,
	FOREIGN KEY (id_publication) REFERENCES publications (id_publication),
    FOREIGN KEY (id_user) REFERENCES users (id_user),
	FOREIGN KEY (id_emote) REFERENCES emote (id_emote),
	UNIQUE (id_publication,id_user, id_emote) 
);



-- Triggers & Fonctions
DROP PROCEDURE IF EXISTS pw_projet.insert_reac;

DELIMITER //
CREATE PROCEDURE pw_projet.insert_reac (IN in_publication_id INT,IN in_user_id INT,IN in_emote_id INT) 
BEGIN 
    DECLARE nbreac INTEGER default 0;
    DECLARE emote integer default 0;
    Select count(*) into nbreac
        from pw_projet.reactions
        where id_publication=in_publication_id
        and id_user=in_user_id; 

    IF nbreac > 0 then

        select id_emote into emote
            from pw_projet.reactions
            where id_publication=in_publication_id
            and id_user=in_user_id limit 1;
            
        delete
        from pw_projet.reactions
        where id_publication=in_publication_id
            and id_user=in_user_id;
            
        IF emote != in_emote_id THEN

                INSERT INTO pw_projet.reactions (id_publication, id_user, id_emote)
                VALUES (in_publication_id,
                        in_user_id,
                        in_emote_id);
        END IF;

    ELSE
    
    INSERT INTO pw_projet.reactions (id_publication, id_user, id_emote)
VALUES (in_publication_id,
        in_user_id,
        in_emote_id);  
    END IF;
END //
DELIMITER ;

DROP trigger IF EXISTS pw_projet.pub_everyone;
DELIMITER //
CREATE TRIGGER pw_projet.pub_everyone before INSERT ON pw_projet.publications
FOR EACH ROW
BEGIN
    DECLARE position INTEGER;
    DECLARE nom_mention VARCHAR(50);
	DECLARE pub BOOLEAN;    
    
    SET pub = false;
    SET position = LOCATE('@', NEW.publication) ;
    
    WHILE position > 0 DO

        SET nom_mention = SUBSTRING(CONCAT(NEW.publication,' '), position, LOCATE(' ' , CONCAT(NEW.publication,' '), position ) - position );
		IF nom_mention = '@everyone' THEN
			SET NEW.is_public=true;
		END IF;
		SET position = LOCATE('@', CONCAT(NEW.publication,' '),position+1) ;
    END WHILE;
    
END//
DELIMITER ;

DROP trigger IF EXISTS pw_projet.pub_mention;
DELIMITER //
CREATE TRIGGER pw_projet.pub_mention AFTER INSERT ON pw_projet.publications
FOR EACH ROW
BEGIN
    DECLARE position INTEGER;
    DECLARE nom_mention VARCHAR(50);
    
    SET position = LOCATE('@', NEW.publication) ;
    
    WHILE position > 0 DO
        SET nom_mention = SUBSTRING(CONCAT(NEW.publication,' '), position, LOCATE(' ' , CONCAT(NEW.publication,' '), position ) - position ) ;
		IF nom_mention != '@everyone' then
			INSERT INTO pw_projet.mentions (id_publication, login_user) VALUES (NEW.id_publication,nom_mention);
		END IF;
		SET position = LOCATE('@', CONCAT(NEW.publication,' '),position+1) ;
    END WHILE;
    
END//
DELIMITER ;


DROP trigger IF EXISTS pw_projet.pub_hashtag;
DELIMITER //
CREATE TRIGGER pw_projet.pub_hashtag AFTER INSERT ON pw_projet.publications
FOR EACH ROW
BEGIN
    DECLARE position INTEGER;
	DECLARE nom_hashtag VARCHAR(50);
    
    SET position = LOCATE('#', NEW.publication) ;
    
    WHILE position > 0 DO

        SET nom_hashtag = SUBSTRING(CONCAT(NEW.publication,' '), position,LOCATE(' ' , CONCAT(NEW.publication,' '), position ) - position );
        
		INSERT INTO pw_projet.hashtags (id_publication, hashtag) VALUES (NEW.id_publication, nom_hashtag);
        
		SET position = LOCATE('#', CONCAT(NEW.publication,' '), position+1) ;
    END WHILE;
    
END//
DELIMITER ;

-- procedures
DROP PROCEDURE IF EXISTS pw_projet.delete_pub;

DELIMITER //
CREATE PROCEDURE pw_projet.delete_pub (IN in_publication_id INT, IN in_id_usr INT) BEGIN
DELETE
FROM pw_projet.mentions
WHERE id_publication=in_publication_id;
    DELETE
    FROM pw_projet.hashtags WHERE id_publication=in_publication_id;
    DELETE
    FROM pw_projet.reactions WHERE id_publication=in_publication_id;
    DELETE
    FROM pw_projet.retweets WHERE id_publication=in_publication_id;
    DELETE
    FROM pw_projet.publications WHERE id_publication=in_publication_id
    AND ID_user = in_id_usr; END //
DELIMITER ;
-- DROP PROCEDURE IF EXISTS pw_projet.delete_pub;
-- DELIMITER //
-- CREATE PROCEDURE pw_projet.delete_pub (IN in_publication_id INT) 
-- BEGIN
-- DELETE
-- FROM pw_projet.mentions
-- WHERE id_publication=in_publication_id;
--     DELETE
--     FROM pw_projet.hashtags WHERE id_publication=in_publication_id;
--     DELETE
--     FROM pw_projet.publications WHERE id_publication=in_publication_id; END //
-- DELIMITER ;

-- Inserts 10 users 
INSERT INTO pw_projet.users (login_user, password_user, avatar, email, introduction)
VALUES
    ('Goku', 'Goku', 'goku.jpeg', 'goku@projetpw.com', 'Bonjour ! Je suis Goku, je suis hyper fort, bref je suis invincible, me testes même pas! cf CEO of Pizza Rolls! Muscles. Smiles. Posts!'),
    ('Bulma', 'Bulma', 'bulma.jpeg', 'bulma@projetpw.com', 'Hey, are you looking at me? I am a Cat lover. I believe that happiness depends upon ourselves. '),
    ('Krillin', 'Krillin', 'krillin.jpeg', 'krillin@projetpw.com', 'Yo, this is my page. A guy smarter than Goku. Imagination is more important than knowledge'),
    ('Piccolo', 'Piccolo', 'piccolo.jpeg', 'piccolo@projetpw.com', 'Remember who I am. If you don’t like the road you’re walking, start paving another one'),
    ('Gohan', 'Gohan', 'gohan.jpeg', 'gohan@projetpw.com', 'Get stronger! A man cannot be comfortable without his own approval. Oh, I am Gohan by the way'),
    ('Vegeta', 'Vegeta', 'vegeta.jpeg', 'vegeta@projetpw.com', 'A man who dream to be a super Saiyaman. To Remember : You can be the lead in your own life. Just do it!'),
    ('MasterRoshi', 'MasterRoshi', 'masterroshi.jpeg', 'masterroshi@projetpw.com', 'The way to get started is to quit talking and begin doing. That is how we learn and be strong!'),
    ('Trunks', 'Trunks', 'trunks.jpeg', 'trunks@projetpw.com', 'Making the world a comfier place for everybody. World peace lover, yeah, it is me'),
    ('Android18', 'Android18', 'android18.jpeg', 'android18@projetpw.com', 'Famous youtuber, make up, foody, travel, design. The only impossible journey is the one you never begin.'),
    ('Yamcha', 'Yamcha', 'yamcha.jpeg', 'yamcha @projetpw.com', 'My name is yamcha, not matcha. I love Peace and Ice Cream, matcha flavor'); 

-- Inserts publications
INSERT INTO pw_projet.publications(id_user, create_date, publication) 
VALUES
    (1, now(), 'Sometimes life is too uncertain to have regrets.'),
    (1, now(), '@Bulma Let s give it everything we have got!'),
    (1, now(), 'Me personally, I like to work and train. @Trunks And you?'),

    (2, now(), '@everyone @Gohan and @Krillin have deserted me in the middle of nowhere and what is worse I am running out of lipgloss. You have got to save me!!!'),    
    (2, now(), 'How dare you, @Vegeta Every time you poke your little, geeky face in front of @Trunks you make him cry!'),
    (2, now(), 'Look @Goku you can go out in public and have your hair sticking out in every direction IF you want to, but not me!'),
    (2, now(), 'What kind of a woman do you think I am? @everyone'),
    (2, now(), '@everyone Somebody help me! I am too young and pretty to die! #toopretty #actress'),

    (3, now(), '@Goku @Gohan and @Vegeta could not even beat this monster, so what chance do I have against him? But IF I do not try... then there is no one left to protect my friends! #friendship #loveyouall #beaman'),
    (3, now(), 'I summon up the Dragon...I command you now! Hear my howl...to make my wish come true! #dragonball @everyone #wishcometrue'),

    (4, now(), '@everyone My greatest opponent is myself. If I can discover what my weaknesses are, then I will be an even stronger fighter than I already am! #thebest #parfait'),
    (4, now(), '@everyone Sometimes, we have to look beyond what we want and do what’s best. #greatquoteever'),
    
    (5, now(), '@everyone I do not understand why there is another @Piccolo but it seems I cannot avoid fighting #selfcontrol'),
    (5, now(), 'I am the Great Saiyaman! @eveyone'),
    (5, now(), '@Piccolo can I have a new outfit just like yours? You were my first teacher.'),
    

    (6, now(), 'Silly @Gohan Do you really think you have a chance against a Super Saiyan like me? Your brain must be malfunctioning. #iamthebest #saiyan #Iwillbeatyou '),
    (6, now(), 'Never send a boy to do a mans job @everyone #iamaman #superhero #beaman '),
    
    (7, now(), '@everyone Yes, We’ll spend this hour on good old napping. Work hard, Study well and eat and sleep plenty… that’s the turtle hermit way to learn! #workhard #playhard '),
    (7, now(), '@Krillin wake up! It is time to get started! Early bird and the worm. Uh, you know the saying...#grandpastyle'),
    (7, now(), 'Well, I am like a turtle and you are part fish @Android18 @Bulma so what do you say you and me go inside and learn more about our species?'),
    (7, now(), '@Goku you are FROM outer space! #important'),
    
    (8, now(), 'you are about to find out what it is like to fight a real Super Saiyan...and I am not talking about @Goku #supersaiyan'),
    (8, now(), 'Is anyone there? I feel so sad @everyone'),

    (9, now(), 'Not too shabby, @Krillin'),
    (9, now(), 'Do not let me down, old man! @MasterRoshi'),
    (9, now(), 'You kids need to learn some manners! @Yamcha @Goku @Gohan @Trunks #manners #bepolite'),
    (9, now(), 'see you all later! @everyone @Gohan'),

    (10, now(), 'Phew! Thanks, @Gohan #bff'),
    (10, now(), 'Woooooo! @everyone Thanks to @Gohan I got promoted! #bff #top #workhard #promoted'),

    (10, now(), '@eveyone check out this : You definitely seems a lot stronger than you were before, @MasterRoshi #myteacher #respect');

INSERT INTO pw_projet.retweets(id_publication, id_user, retweet_date, retweets) 
VALUES
    (1, 2, NOW(), 'Yes! Let s do this!'),
    (1, 3, NOW(), 'Cannot agree with you more'),
    (1, 4, NOW(), 'This is the best quote ever!'),

    (2, 8, NOW(), 'What a wonderful behavior. '),
    (2, 9, NOW(), 'Donnot be so silly!'),
    
    (3, 3, NOW(), 'I like to stay at home, chill.'),

    (4, 3, NOW(), 'Come on, it was long time age, stop saying this'),

    (6, 1, NOW(), 'Euh...'),
    (6, 6, NOW(), 'So true, cannot not agree with you more. Let me help you!'),
    (6, 9, NOW(), 'As pretty as me. Grils power!'),

    (7, 6, NOW(), 'Give them some lessons, bring it on!'),

    (8, 1, NOW(), 'Congrat!'),
    (8, 2, NOW(), 'What a giant dragon.'),
    (8, 4, NOW(), 'So...what is your dream?'),
    (8, 5, NOW(), 'Congratttttttt!'),
    (8, 6, NOW(), 'Young man, I am so proud of you!'),
    (8, 9, NOW(), 'Wowwwwww, congrat!'),
    (8, 10, NOW(), 'Best wish ever, hein?'),

    (9, 2, NOW(), 'Work hard!'),
    (9, 3, NOW(), 'Fighting!'),
    (9, 5, NOW(), 'Let s train together!'),
    
    (10, 1, NOW(), 'Old story!'),
    (10, 2, NOW(), 'Cannot agree with you more!'),
    (10, 3, NOW(), 'Best quote!'),
    (10, 5, NOW(), 'Indeed'),
    (10, 6, NOW(), 'What a master speech, how old are you, kid?'),
    (10, 7, NOW(), 'That is so true, share!'),
    (10, 8, NOW(), 'You are nailed it!'),

    (11, 1, NOW(), 'Congrat!'),
    (11, 2, NOW(), 'What a giant dragon.'),
    
    (13, 4, NOW(), 'Cannot agree with you more!'),
    (13, 7, NOW(), 'A manm hein?'),
    (13, 9, NOW(), 'Just do it'),

    (15, 5, NOW(), 'Donnot be lazy if you want to be strong!'),

    (16, 2, NOW(), 'No way!'),
    (16, 9, NOW(), 'Noooooooooooo!'),
    
    (17, 1, NOW(), 'I know, so proud of me!'),

    (21, 2, NOW(), 'Give them some lessons, great job!'),
    (21, 7, NOW(), 'I want to be your student too!'),
    (21, 8, NOW(), 'You kids, hahaha!'),

    (23, 7, NOW(), 'Yes!'),
    (23, 1, NOW(), 'He is my teacher and my friend ever!');


INSERT INTO pw_projet.abonnements(id_user, id_user_follower)
VALUES
    (1, 1),
    (1, 2),
    (1, 3),
    (1, 4),
    (1, 5),
    (1, 6),
    (1, 7),
    (1, 9),
    (1, 10),
    
    (2, 2), 
    (2, 1),
    (2, 3),
    (2, 4),
    (2, 6),
    (2, 7),
    (2, 10),
    
    (3, 3),
    (3, 1),
    (3, 2),
    (3, 6),
    (3, 9),
    
    (4, 4),
    (4, 3),
    (4, 5),
    (4, 7),
    (4, 8),
    (4, 9),
    (4, 10),

    (5, 5),
    (5, 1),
    (5, 6),
    (5, 7),
    (5, 8),
    (5, 9),

    (6, 6),
    (6, 1),
    (6, 2),
    (6, 5),
    (6, 9),

    (7, 7),
    (7, 2),
    (7, 3),
    (7, 4),
    (7, 5),
    (7, 6),
    (7, 1),
    (7, 8),
    (7, 9),
    (7, 10),

    (8, 8),
    (8, 1),
    (8, 2),
    (8, 6),
    (8, 10),

    (9, 9),
    (9, 1),
    (9, 2),
    (9, 3),
    (9, 4),
    (9, 5),
    (9, 6),
    (9, 7),
    (9, 8),
    (9, 10),

    (10, 10),
    (10, 1),
    (10, 2),
    (10, 4),
    (10, 5),
    (10, 8),
    (10, 9);

INSERT INTO pw_projet.emote (id_emote, emote)
VALUES
    (1, 'like'), 
    (2, 'dislike');

INSERT INTO pw_projet.reactions(id_publication, id_user, id_emote) 
VALUES
    (1, 2, 1),
    (1, 3, 1),
    (1, 4, 2),
    (1, 5, 1),
    (1, 6, 1),
    (1, 7, 1),
    (1, 9, 1),
    (1, 10, 1),

    (2, 2, 1),
    (2, 3, 1),
    (2, 4, 1),
    (2, 7, 1),
    
    (3, 8, 1),

    (4, 1, 1),
    (4, 3, 1),
    (4, 4, 2),
    (4, 5, 1),
    (4, 8, 2),
    
    (5, 6, 1),
    (5, 7, 1),
    
    (6, 9, 1),
    (6, 10, 1),
    (6, 2, 1),
    
    (7, 3, 1),
    (7, 4, 2),
    (7, 5, 1),
    (7, 8, 1),
    (7, 9, 2),
    (7, 10, 1),
    
    (8, 1, 1),
    (8, 2, 1),
    (8, 4, 1),
    (8, 5, 2),
    (8, 6, 1),
    (8, 7, 1),
    (8, 8, 1),
    (8, 9, 1),
    (8, 10, 1),

    (9, 7, 1),
    (10, 9, 1),
    (11, 10, 1),

    (12, 1, 2),
    (12, 5, 2),
    (12, 6, 1),

    (13, 4, 2),
    (13, 5, 2),
    (13, 1, 1),
    (13, 7, 1),
    (13, 8, 1),

    (14, 1, 1),
    (14, 2, 1),
    (14, 3, 2),
    (14, 4, 1),
    (14, 9, 1),

    (15, 1, 1),
    (15, 3, 1),
    (15, 6, 1),

    (16, 1, 1),
    (16, 2, 2),
    (16, 9, 2),

    (17, 8, 1),
    (17, 1, 1),
    (17, 2, 1),
    (17, 3, 2),
    (17, 4, 1),
    (17, 5, 2),
    (17, 10, 1),

    (18, 2, 1),
    (18, 3, 2),
    (18, 1, 1),
    (18, 10, 1),
    (18, 5, 1),
    (18, 6, 2),
    (18, 8, 1),
    (18, 9, 2),

    (19, 1, 1),
    (19, 2, 1),
    (19, 3, 2),
    (19, 4, 1),
    (19, 9, 1),

    (23, 1, 1),
    (23, 2, 1),
    (23, 3, 2),
    (23, 4, 1),
    (23, 6, 1),
    (23, 9, 1),
    (23, 10, 1),
    
    (27, 1, 1),
    (27, 2, 1),
    (27, 6, 2),
    (27, 4, 1),
    (27, 8, 1),

    (29, 2, 1),
    (29, 3, 2),
    (29, 1, 1),
    (29, 4, 1),
    (29, 5, 1),
    (29, 6, 2),
    (29, 8, 1),
    (29, 9, 2);



-- -- verif 
-- SELECT * FROM pw_projet.users;
-- SELECT * FROM pw_projet.abonnements;
-- SELECT * FROM pw_projet.publications;
-- SELECT * FROM pw_projet.mentions;
-- SELECT * FROM pw_projet.hashtags;
-- SELECT * FROM pw_projet.retweets;
-- SELECT * FROM pw_projet.emote;
-- SELECT * FROM pw_projet.reactions;
-- SELECT * FROM pw_projet.hashtags WHERE hashtag='#actress';
