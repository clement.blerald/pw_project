const express = require('express');
const router = express.Router();
const followerlist = require('../services/followerList');


router.get('/', async function (req, res, next) {
    try {
        res.json(await followerlist.getFollowerList(req.query.login));

    } catch (err) {
        console.log(`Error to get get follower list`, err.message);
        next(err);
    }
})

module.exports = router;