const express = require('express');
const router = express.Router();
const followerlist = require('../services/userNOTFollower');


router.get('/', async function (req, res, next) {
    try {
        res.json(await followerlist.userNOTFollower(req.query.id));

    } catch (err) {
        console.log(`Error to get not follower list`, err.message);
        next(err);
    }
})

module.exports = router;