const express = require('express');
const router = express.Router();
const userlist = require('../services/allUserList');

router.get('/', async function (req, res, next) {
    try {
        res.json(await userlist.getUserList());

    } catch (err) {
        console.log(`Error to get user list`, err.message);
        next(err);
    }
})

module.exports = router;