const express = require('express');
const router = express.Router();
const userId = require('../services/userId');

router.get('/', async function (req, res, next) {
    try {
        res.json(await userId.getById(req.query.uid))

    } catch (err) {
        console.log(`Error to get user id`, err.message);
        next(err);
    }
})


module.exports = router;