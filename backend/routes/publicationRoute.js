const express = require('express');
const router = express.Router();
const publications = require('../services/publications');


router.get('/', async function (req, res, next) {
    try {
        //const result = await identifications.login();
        res.json(await publications.getPublications()); //put result of ident.login in json form
    } catch (err) {
        console.log(`Error to get all publications`, err.message);
        next(err);
    }
})

module.exports = router;