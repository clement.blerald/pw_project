const express = require('express');
const router = express.Router();
const identifications = require('../services/identifications');

router.get('/', async function (req, res, next) {
    try {
        res.json(await identifications.login(req.query.login, req.query.password)); //put result of ident.login in json form
    } catch (err) {
        console.log(`Error to identify`, err.message);
        next(err);
    }
})

module.exports = router;