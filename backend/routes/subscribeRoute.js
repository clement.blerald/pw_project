const express = require('express');
const router = express.Router();
const sub = require('../services/subscribeTo');

router.get('/', async function (req, res, next) {
    try {

        res.json(await sub.subscribeTo(req.query.login_user, req.query.follower));

    } catch (err) {
        console.log(`Error to subscribe new users`, err.message);
        next(err);
    }
})

module.exports = router;