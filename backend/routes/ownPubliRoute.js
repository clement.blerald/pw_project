const express = require('express');
const router = express.Router();
const OwnPubli = require('../services/getOwnPubli');


router.get('/', async function (req, res, next) {
    try {
        res.json(await OwnPubli.getOwnPubli(req.query.id));

    } catch (err) {
        console.log(`Error to get user own publication`, err.message);
        next(err);
    }
})

module.exports = router;