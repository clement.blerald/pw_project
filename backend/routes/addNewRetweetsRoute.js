const express = require('express');
const router = express.Router();
const newRetweets = require('../services/addRetweets');

router.get('/', async function (req, res, next) {
    try {
        res.json(await newRetweets.addRetweets(req.query.idPublication, req.query.idUser, req.query.retweet));
    } catch (err) {
        console.log(`Error to add retweets`, err.message);
        next(err);
    }
})

module.exports = router;