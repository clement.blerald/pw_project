const express = require('express');
const router = express.Router();
const newPubli = require('../services/newPubli');

router.get('/', async function (req, res, next) {
    //encodeURL
    try {
        res.json(await newPubli.insertPubli(req.query.publication.substring(1), req.query.login));
    } catch (err) {
        console.log(`Error to insert new publications`, err.message);
        next(err);
    }
})

module.exports = router;