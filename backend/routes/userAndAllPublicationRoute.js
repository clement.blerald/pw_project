const express = require('express');
const router = express.Router();
const userandallpublications = require('../services/loginPublication');


router.get('/', async function (req, res, next) {
    try {
        //const result = await identifications.login();
        res.json(await userandallpublications.getUserAndAllPublications(req.query.login, req.query.login)); //put result of ident.login in json form


    } catch (err) {
        console.log(`Error to get User And All Publications`, err.message);
        next(err);
    }
})

module.exports = router;