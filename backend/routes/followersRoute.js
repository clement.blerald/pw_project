const express = require('express');
const router = express.Router();
const followerlist = require('../services/userFollower');


router.get('/', async function (req, res, next) {
    try {
        res.json(await followerlist.userFollower(req.query.id));

    } catch (err) {
        console.log(`Error to get follower`, err.message);
        next(err);
    }
})

module.exports = router;