const express = require('express');
const router = express.Router();
const newReaction = require('../services/addLikeDislike');

router.get('/', async function (req, res, next) {
    try {

        res.json(await newReaction.addReactions(req.query.idPublication, req.query.idUser, req.query.idEmote));

    } catch (err) {
        console.log(`Error to add reactions`, err.message);
        next(err);
    }
})

module.exports = router;