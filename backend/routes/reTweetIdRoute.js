const express = require('express');
const router = express.Router();
const reTweetId = require('../services/reTweetId');

router.get('/', async function (req, res, next) {
    try {
        res.json(await reTweetId.getReTweetId(req.query.idPublication));

    } catch (err) {
        console.log(`Error to get get user id of retweet post`, err.message);
        next(err);
    }
})

module.exports = router;