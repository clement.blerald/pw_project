const express = require('express');
const router = express.Router();
const deletedPost = require('../services/deletedPost');

router.get('/', async function (req, res, next) {
    try {

        res.json(await deletedPost.deletedPosts(req.query.idPublication, req.query.idUser));

    } catch (err) {
        console.log(`Error to deleted post`, err.message);
        next(err);
    }
})


module.exports = router;