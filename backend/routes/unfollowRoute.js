const express = require('express');
const router = express.Router();
const unfollow = require('../services/unFollow');


router.get('/', async function (req, res, next) {
    try {
        res.json(await unfollow.unFollow(req.query.followerId, req.query.userIdToUnfollow));

    } catch (err) {
        console.log(`Error to unfollow user`, err.message);
        next(err);
    }
})

module.exports = router;