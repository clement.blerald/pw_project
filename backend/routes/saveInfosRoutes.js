const express = require('express');
const router = express.Router();
const Infos = require('../services/updateInfos');

router.post('/api/saveInfos', async function (req, res, next) {

    try {
        res.json(await Infos.updateInfos(req.body.data));

    } catch (err) {

        console.log(`Error to update infos`, err.message);
        next(err);
    }
})

module.exports = router;