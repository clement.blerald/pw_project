const express = require('express');
const cors = require('cors');
const app = express();
const port = 3000;
const identificationsRouter = require('./routes/identificationsRoute');
const publicationsRouter = require('./routes/publicationRoute');
const userallpublicationsRouter = require('./routes/userAndAllPublicationRoute');
const alluserlist = require('./routes/userListRoute');
const followerlist = require('./routes/followerListRoute');
const getbyid = require('./routes/userIdRoute');
const NewPubli = require('./routes/insertnewPubliRoute');
const followers = require('./routes/followersRoute');
const retweetposts = require('./routes/reTweetIdRoute');
const notFollower = require('./routes/notFollowerRoute');
const getOwnPubli = require('./routes/ownPubliRoute');
const saveInfos = require('./routes/saveInfosRoutes');
const subscribeTo = require('./routes/subscribeRoute');
const newRetweets = require('./routes/addNewRetweetsRoute');
const newReactions = require('./routes/addLikeDislikeRoute');
const unFollow = require('./routes/unfollowRoute');
const deletedPost = require('./routes/deletedPostRoute');


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => {
    res.json({ 'message': 'good' });
});


//deleted post of user connected
app.use('/api/deletedPost', deletedPost);

//unfollow user
app.use('/api/unFollow', unFollow);

//save change of introduciton, email and login_user
app.post('/api/saveInfos', saveInfos);

//subscribe user
app.use('/api/subscribeto', subscribeTo);

//login route
app.use('/api/identifications', identificationsRouter);

//publications route
app.use('/api/publications', publicationsRouter);

//user, his follower and Allpublication @user or @everyone
app.use('/api/userandallpublications', userallpublicationsRouter);

//get whole user list
app.use('/api/alluserlist', alluserlist);

//get whole follower list
app.use('/api/followerlist', followerlist);

//write a new post
app.use('/api/insertNewPubli', NewPubli);

//get user by id
app.use('/api/getbyid', getbyid);

//get follower of user connected
app.use('/api/getFollowers', followers);

//get not follower
app.use('/api/getNOTFollowers', notFollower);

//get posts of user connected
app.use('/api/getOwnPubli', getOwnPubli);

//get retweetid and posts
app.use('/api/retweetpost', retweetposts);

//get retweetid and posts
app.use('/api/addNewRetweets', newRetweets);

//get retweetid and posts
app.use('/api/addLikeDislike', newReactions);

/* Error handler middleware */
app.use((err, req, res, next) => {
    const statusCode = err.statusCode || 500;
    console.error(err.message, err.stack);
    res.status(statusCode).json({ 'message': err.message });
    return;
});

app.listen(port, () => {
    console.log(`backend servor is running at http://localhost:${port}`)
});