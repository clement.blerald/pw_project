require('dotenv').config();
const env = process.env; //get variable in the file env via process
const config = {
    db: { //ident of database, get from file env
        host: env.DB_HOST,
        user: env.DB_USER,
        password: env.DB_PASSWORD,
        database: env.DB_NAME
    }
}

module.exports = config;
