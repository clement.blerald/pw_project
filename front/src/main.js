import VueRouter from 'vue-router'
import routes from './routes/routes'
import Vue from 'vue'
import App from './App.vue'
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

//for using VueRouter, BootstrapVue and icons
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.use(VueRouter);


Vue.config.productionTip = false

const router = new VueRouter({
  mode: 'history',
  routes, // short for routes: routes
  linkExactActiveClass: 'nav-item active',
});

new Vue({
  el: '#app',  //el create application from App.vue
  render: h => h(App),
  router,
});
