export async function subscribeTo(login_user, follower) {

    try {
        const reponse = await fetch(
            `http://localhost:3000/api/subscribeto?login_user=${login_user}&follower=${follower}`, {
            method: 'GET', //GET for get the information of user
        });
        const result = await reponse.json();
        return result; //get json
    } catch (e) {
        return { err: e }
    }
}
