
export async function login(username, password) {
    try {

        const resUser = await fetch(`http://localhost:3000/api/identifications?login=${username}&password=${password}`, {
            method: 'GET', //GET for get the information of user

        })

        const result = (await resUser.json())[0]; //return elemnt[0] 
        return result; //get json


    } catch (e) {
        return { err: e }
    }
}

export async function getUserById(userId) {
    try {
        const resGetById = await fetch(`http://localhost:3000/api/getbyid?uid=${userId}`, {
            method: 'GET', //GET for get the information of user
        })

        const result = await resGetById.json();
        return result[0]; //get json


    } catch (e) {
        return { err: e }
    }
}