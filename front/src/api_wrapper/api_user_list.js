export async function getEachUser() {
    try {
        const resEachUser = await fetch(`http://localhost:3000/api/alluserlist`, {
            method: 'GET', //GET for get the information of user

        })

        const result = await resEachUser.json();
        return result; //get json
    } catch (e) {
        return { err: e }
    }
}
