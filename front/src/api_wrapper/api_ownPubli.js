export async function getOwnPubli(id) {
    try {
        const followers = await fetch(
            `http://localhost:3000/api/getOwnPubli?id=${id}`, {
            method: 'GET', //GET for get the information of user
        });
        const result = await followers.json();
        return result; //get json
    } catch (e) {
        return { err: e }
    }
}
