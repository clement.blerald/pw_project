export async function getRetweetPost(idpublication) {
    try {
        const resUserFollower = await fetch(`http://localhost:3000/api/retweetpost?idPublication=${idpublication}`, {
            method: 'GET', //GET for get the information of user

        })
        const result = await resUserFollower.json();

        return result; //get json
    } catch (e) {
        return { err: e }
    }
}