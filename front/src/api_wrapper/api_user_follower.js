export async function getUserAndHisFollower(login) {
    try {
        const resUserFollower = await fetch(`http://localhost:3000/api/followerlist?login=${login}`, {
            method: 'GET', //GET for get the information of user

        })
        const result = await resUserFollower.json();

        return result; //get json
    } catch (e) {
        return { err: e }
    }
}