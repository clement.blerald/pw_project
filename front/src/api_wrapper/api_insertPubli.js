export async function insertNewPubli(publication, login) {
    publication = publication.replaceAll("#", "%23");
    //replace # by %23 because url doesn't take any word after #
    try {
        const insertNewPubli = await fetch(
            `http://localhost:3000/api/insertNewPubli?publication=${publication}&login=${login}`, {
            method: 'GET', //GET for get the information of user
        });
        const result = await insertNewPubli.json();
        return result; //get json
    } catch (e) {
        return { err: e }
    }
}
