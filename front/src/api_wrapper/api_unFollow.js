export async function unFollow(followerId, userIdToUnfollow) {
    //replace # by %23 because url doesn't take any word after #
    try {
        const newRetweets = await fetch(
            `http://localhost:3000/api/unFollow?followerId=${followerId}&userIdToUnfollow=${userIdToUnfollow}`, {
            method: 'GET', //GET for get the information of user
        });
        const result = await newRetweets.json();
        return result; //get json
    } catch (e) {
        return { err: e }
    }
}
