export async function getTOFollower(id) {
    try {
        const followers = await fetch(
            `http://localhost:3000/api/getNOTFollowers?id=${id}`, {
            method: 'GET', //GET for get the information of user
        });
        const result = await followers.json();
        return result; //get json
    } catch (e) {
        return { err: e }
    }
}
