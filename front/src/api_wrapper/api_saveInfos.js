import $ from 'jquery';

export async function saveInfos(data) {

  data = JSON.stringify(data);
  try {
    var result = await $.post(
      "http://localhost:3000/api/saveInfos",
      { data },
      (res) => {
        //handle servor response
        console.log(res);
      }
    );
    return result; //get json
  } catch (e) {
    return { err: e }
  }
}
