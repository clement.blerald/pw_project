export async function addNewReactions(idPublication, idUser, idEmote) {
    try {
        const newRetweets = await fetch(
            `http://localhost:3000/api/addLikeDislike?idPublication=${idPublication}&idUser=${idUser}&idEmote=${idEmote}`, {
            method: 'GET', //GET for get the information of user
        });

        const result = await newRetweets.json();
        return result; //get json
    } catch (e) {
        return { err: e }
    }
}
