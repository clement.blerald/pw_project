export async function deletedPosts(idPublication, idUser) {
    try {
        const newRetweets = await fetch(
            `http://localhost:3000/api/deletedPost?idPublication=${idPublication}&idUser=${idUser}`, {
            method: 'GET', //GET for get the information of user
        });

        const result = await newRetweets.json();
        return result; //get json
    } catch (e) {
        return { err: e }
    }
}
