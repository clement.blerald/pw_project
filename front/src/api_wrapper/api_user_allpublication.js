export async function getUserAndEveryonePublication(login) {
    try {
        const resEveryonePublication = await fetch(`http://localhost:3000/api/userandallpublications?login=${login}`, {
            method: 'GET', //GET for get the information of user

        })

        const result = await resEveryonePublication.json();

        return result; //get json
    } catch (e) {
        return { err: e }
    }
}
