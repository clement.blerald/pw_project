import Home from '../components/Home.vue';
import Profile from '../components/Profile';


const routes = [
  {
    path: '/',
    name: 'homepage',
    component: Home
  },

  {
    path: '/profile',
    name: 'profilepage',
    component: Profile
  }
];

export default routes;
