Programmation Web L3 2020-2021
======================
## Membres du groupe 

- SU LiFang : 21957835
- BLERALD Clément : 71800092

### Présentation Vidéo 

- Diponible en ligne : https://youtu.be/tlRBSLXlvzM
- Rendu Final : branch `master`
    
### Installation

- `backend`
    - npm install : express, mysql , mysql2, nodemon, dotenv, cors
- `front`
    - npm install : vue, vue/cli, bootstrap bootstrap-vue, js-cookie --save

#### Exécution

- `databases.sql` doit être exécuté via root afin de créer et et initialiser les tables la base de données.
    - `mysql -u root`
    - `source /<chemin absolu>/backend/databases.sql`
- Dans le répertoire `backend`, on peut exécuter le backend via `npm start` 
- Dand le répertoire `front`, on peut exécuter le backend via `npm start`
- Navigateur Conseillé : `Firefox` 
    - Test effétué sur la version 88.0.1 (64-bit)


### Structure du Projet

- **Backend** :
    - fichier **index.js** : serveur du projet
    - Dossier **routes** : contient toutes les routes du utilisés par le frontend pour communiquer avec le backend
    - Dossier **services** : contient toutes les requêtes nécessaire à la communication entre la base Mysql et le serveur
    - fichier `databases.sql` : contient le script SQL pour créer les triggers, les fonctions, créer et remplir les tables avec des exemples de tests
        - Tables
            - users : contient 10 users pré-définits. Chaque user a son propre login, password, email et une introduction
            - publications : contient les publications de chaque user
            - abonnements : les abonnements entre followers et utilisateur 
            - retweets : les réponses sur les publications
            - reactions : like et dislike sur les publications
            - emotes : contient 2 types d'emote : like et dislike (design évolutif)
            - hashtags : list les hastag lié aux publications
            - mentions : list les mentions des publications
        - Fonctions
            - insert_reac : Fonction insert des reactions gérant automatiquement les précédentes reactiones engrestrées sur une publication
        - Triggers
            - pub_everyone : transforme le booléan is_public de la table de publication en `True` si on insert une publication contenant `@everyone`
            - pub_mention : pour remplir la table `mentions` automatiquement dès qu'il y a une nouvelle publication
            - pub_hashtag : pour remplir la table `hashtags` automatiquement dès qu'il y a une nouvelle publication
    - fichier `package-lock.json` et `package.json` : fichier de configuration créé par npm. Définition des commandes pour exécuter le programme
    - fichier `.env` : Données accèes à la base de données Mysql (environment variables)
    - fichier `config.js` : Configurations des identifiants de la base de données, via les données de env
    
- **Frontend** :
    - Dans `src` :
        - Dossier **api_wrapper** : contient les api calls (ajax ou fetch) vers le serveur
        - Dossier **assets** : contient toutes les images du site
        - Dossier **components** :
            - `NavBar.vue` : Navbar de l'ensemble des pages du site (en haut), definition des bouttons selon l'utilisateur connecté
            - `Home.vue` :Page principale du site (non connecté et connecté)
            - `Profile.vue` : Page profile de l'utilisateur connecté
        - Dossier **routes** : contient les deux routes définies pour retourner sur la page "Home" ou sur la page "profile"
        - fichier `main.js` : la classe princiaple de vue.js
        - fichier `App.vue` : la classe d'affichage


### Problème Rencontré
- Compatibilité inter navigateur

### Arborescence du projet
```
.
├── README.md
├── backend
|    ├── config.js
|    ├── .env
|    ├── index.js
|    ├── package-lock.json
|    ├── package.json
|    ├── database.sql
|    ├── routes
|    │   ├── deletedPostRoute.js        
|    │   ├── addLikeDislikeRoute.js
|    │   ├── addNewRetweetsRoute.js
|    │   ├── followerListRoute.js
|    │   ├── followersRoute.js
|    │   ├── identificationsRoute.js
|    │   ├── insertnewPubliRoute.js
|    │   ├── notFollowerRoute.js
|    │   ├── ownPubliRoute.js
|    │   ├── publicationRoute.js
|    │   ├── reTweetIdRoute.js
|    │   ├── saveInfosRoutes.js
|    │   ├── subscribeRoute.js
|    │   ├── userAndAllPublicationRoute.js
|    │   ├── userIdRoute.js
|    │   └── userListRoute.js
|    │   └── unfollowRoute.js
|    └── services
|        ├── deletedPostjs
|        ├── addLikeDislike.js
|        ├── addRetweets.js
|        ├── allUserList.js
|        ├── db.js
|        ├── followerList.js
|        ├── getOwnPubli.js
|        ├── identifications.js
|        ├── loginPublication.js
|        ├── newPubli.js
|        ├── publications.js
|        ├── reTweetId.js
|        ├── subscribeTo.js
|        ├── updateInfos.js
|        ├── userFollower.js
|        ├── userId.js
|        └── userNOTFollower.js
|        └── unFollow.js
|
├── front
    ├── babel.config.js
    ├── package-lock.json
    ├── package.json
    └── src
        ├── App.vue
        ├── api_wrapper
        │   ├── api_deletedPost.js
        │   ├── api_NOTFollower.js
        │   ├── api_addNewRetweets.js
        │   ├── api_addReactions.js
        │   ├── api_calls.js
        │   ├── api_follower.js
        │   ├── api_insertPubli.js
        │   ├── api_ownPubli.js
        │   ├── api_publication.js
        │   ├── api_retweet.js
        │   ├── api_saveInfos.js
        │   ├── api_subscribe.js
        │   ├── api_user_allpublication.js
        │   ├── api_user_follower.js
        │   └── api_user_list.js
        │   └── api_unFollow.js
        ├── assets
        │   └── img/
        ├── components
        │   ├── Home.vue
        │   ├── Navbar.vue
        │   ├── Profile.vue
        │   └── event.js
        ├── main.js
        └── routes
            └── routes.js
```

### Références
- Source d'images : https://www.ranker.com
- b-table : https://bootstrap-vue.org/docs/components/table
- b-modal : https://bootstrap-vue.org/docs/components/modal
- mounted : https://v3.vuejs.org/api/options-lifecycle-hooks.html#beforeupdate 
- event-bus : https://blog.logrocket.com/using-event-bus-in-vue-js-to-pass-data-between-components/
- dotenv : https://www.npmjs.com/package/dotenv